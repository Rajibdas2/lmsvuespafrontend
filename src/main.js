import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import axios from 'axios';

axios.defaults.baseURL = 'http://lms.test:8090/api'
const app = createApp(App);

app.config.globalProperties.$axios = axios;
app.config.globalProperties.productionTip = false;


app.use(router);
app.use(store);

app.mixin({
  created() {
 
    const userInfo = localStorage.getItem('user');
    if (userInfo) {
      const userData = JSON.parse(userInfo);
      this.$store.commit('setUserData', userData);
    }

 
    axios.interceptors.response.use(
      response => response,
      error => {
        if (error.response && error.response.status === 401) {
          this.$store.dispatch('logout');
        }
        return Promise.reject(error);
      }
    );
  }
});


app.mount('#app');
